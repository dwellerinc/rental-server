http = require('http')
twilio = require('twilio')
express = require('express')
bodyParser = require('body-parser')
twilioSid = 'SKa3c1c08cd4da1048063c1de160caf18e'
twilioToken = '4qs2Q9zUk5exGiRqpsA4X24JCcfaElfl'
twilioClient = require('twilio')(twilioSid, twilioToken)
app = express()

server = http.Server(app)

io = require('socket.io')(server)

parse = require('parse')
parse.initialize("Ee2FI97fstyJnca8vv8CBAesQRDhJd2Xf6flFgqw", "PPtqcZ8seAaKaAfOoafB8T1J3oSjU9G29mNQbYEP");


messageResponse = (message, res) ->
  resp = new (twilio.TwimlResponse)
  resp.message ->
    @body message
    return
  res.type 'text/xml'
  res.send resp.toString()
  return

app.use bodyParser.urlencoded(extended: false)
# Twilio will POST to this route when SMS/MMS is received
app.post '/twiml', (req, res) ->
  # Log the data sent by Twilio
  console.log req.body
  #messageResponse 'Hello World.', res
  return

app.post '/receiveSms', (req, res) ->
  console.log req.body
  console.log "RECEIVING SMS!!!!!"
  console.log io.in('test').emit('receive', {message: req.body.Body})

app.post '/smsError', (req, res) ->
  console.log "SMS ERRORRRRRRR !!!!!!!!!"
  console.log req.body

io.on 'connection', (socket) ->
  console.log('connected')
  socket.join('test')
  socket.emit('receive', {test:'test'})
  io.in('test').emit('receive', 'in test')

  socket.on 'disconnect', ->
    console.log('disconnected')

app.post '/receiveEmail', (req, res) ->
  console.log "RECEIVE EMAIL"
  console.log req.body
  console.log req.body['stripped-text']
  io.in('test').emit('receiveEmail', {message: req.body['stripped-text']})

# Launch the HTTP server serving the Express app
server.listen process.env.PORT
